<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cric Stats</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="../../assets/fonts/fontawesome-all.min.css"> -->
    <link rel="stylesheet" href="assets/css/custom-styles.css">
    
</head>

<body id="">
    <header id="page-top">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-12">
                    <!-- <img class="img-responsive" src="../../assets/img/banner/banner.jpeg" alt=""> -->
                    <div class="intro-text">
                        <span class="name">Scroll Down</span>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </header>
    
    <div id="wrapper" class="wrapper">
        <div class="d-flex flex-column" id="content-wrapper">
            
            <div id="content">
                   
                <div class="col-md-12">
                    <div class="tab" role="tabpanel">
                        <!-- Nav tabs -->
                        <ul id="myTab2" role="tablist" class="nav nav-tabs nav-pills with-arrow lined flex-column flex-sm-row text-center">
                            <li class="nav-item flex-sm-fill">
                              <a  id="fixtures-tab" data-toggle="tab" href="#first" class="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0 active" >Fixtures</a>
                            </li>
                            <li class="nav-item flex-sm-fill">
                              <a  id="statistics-tab" data-toggle="tab" href="#second" class="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0">Statistics</a>
                            </li>
                            <li class="nav-item flex-sm-fill">
                              <a  id="analytics-tab" data-toggle="tab" href="#third" class="nav-link text-uppercase font-weight-bold rounded-0">Analytics</a>
                            </li>
                          </ul>
                        <!-- Tab panes -->
                        <div class="tab-content clearfix">
                          <div class="tab-pane active" id="first">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="jumbotron bg-primary">
                                  

 
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="second">
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="jumbotron bg-success">
                                  <h1>T20</h1>
                                  <p>This is profile tab.</p>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="third" style="height: 700px;">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="jumbotron bg-info col-md-12">
                                  <h1>Test</h1>
                                  
                                    





                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    </div>
                        
                </div>
                     
            </div>
            
                   
            <footer class="bg-white sticky-footer">
                <div class="container my-auto" >
                    <div class="text-center my-auto copyright"><span>Copyright © Brand 2021</span></div>
                </div>
            </footer>
        
          </div>
        <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>

</html>
